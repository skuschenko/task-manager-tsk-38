package com.tsc.skuschenko.tm.service;

import com.tsc.skuschenko.tm.api.repository.IBusinessRepository;
import com.tsc.skuschenko.tm.api.service.IBusinessService;
import com.tsc.skuschenko.tm.api.service.IConnectionService;
import com.tsc.skuschenko.tm.enumerated.Status;
import com.tsc.skuschenko.tm.exception.empty.EmptyIdException;
import com.tsc.skuschenko.tm.exception.empty.EmptyNameException;
import com.tsc.skuschenko.tm.exception.empty.EmptyStatusException;
import com.tsc.skuschenko.tm.exception.entity.EntityNotFoundException;
import com.tsc.skuschenko.tm.exception.system.IndexIncorrectException;
import com.tsc.skuschenko.tm.model.AbstractBusinessEntity;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.sql.Connection;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Optional;

public abstract class AbstractBusinessService<E extends AbstractBusinessEntity>
        extends AbstractService<E> implements IBusinessService<E> {

    @NotNull
    public AbstractBusinessService(
            @NotNull final IConnectionService connectionService
    ) {
        super(connectionService);
    }

    @NotNull
    @Override
    @SneakyThrows
    public E changeStatusById(
            @NotNull final String userId, @Nullable final String id,
            @Nullable final Status status
    ) {
        Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        Optional.ofNullable(status).orElseThrow(EmptyStatusException::new);
        @NotNull final Connection connection =
                connectionService.getConnection();
        @NotNull final IBusinessRepository<E> entityRepository =
                getRepository(connection);
        try {
            @NotNull final E entity =
                    entityRepository.changeStatusById(userId, id, status);
            connection.commit();
            return entity;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public E changeStatusByIndex(
            @NotNull final String userId, @Nullable final Integer index,
            @Nullable final Status status
    ) {
        Optional.ofNullable(index).filter(item -> item > -1).
                orElseThrow(() -> new IndexIncorrectException(index));
        Optional.ofNullable(status).orElseThrow(EmptyStatusException::new);
        @NotNull final Connection connection =
                connectionService.getConnection();
        @NotNull final IBusinessRepository<E> entityRepository =
                getRepository(connection);
        try {
            @NotNull final E entity =
                    entityRepository.changeStatusByIndex(userId, index, status);
            connection.commit();
            return entity;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public E changeStatusByName(
            @NotNull final String userId, @Nullable final String name,
            @Nullable final Status status
    ) {
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        Optional.ofNullable(status).orElseThrow(EmptyStatusException::new);
        @NotNull final Connection connection =
                connectionService.getConnection();
        @NotNull final IBusinessRepository<E> entityRepository =
                getRepository(connection);
        try {
            @NotNull final E entity =
                    entityRepository.changeStatusByName(userId, name, status);
            connection.commit();
            return entity;

        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public void clear(@NotNull final String userId) {
        @NotNull final Connection connection =
                connectionService.getConnection();
        @NotNull final IBusinessRepository<E> entityRepository =
                getRepository(connection);
        try {
            entityRepository.clear(userId);
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public E completeById(
            @NotNull final String userId, @Nullable final String id
    ) {
        Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        @NotNull final Connection connection =
                connectionService.getConnection();
        @NotNull final IBusinessRepository<E> entityRepository =
                getRepository(connection);
        try {
            @NotNull final E entity =
                    entityRepository.completeById(userId, id);
            connection.commit();
            return entity;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public E completeByIndex(
            @NotNull final String userId, @Nullable final Integer index
    ) {
        Optional.ofNullable(index).filter(item -> item > -1).
                orElseThrow(() -> new IndexIncorrectException(index));
        @NotNull final Connection connection =
                connectionService.getConnection();
        @NotNull final IBusinessRepository<E> entityRepository =
                getRepository(connection);
        try {
            @NotNull final E entity =
                    entityRepository.completeByIndex(userId, index);
            connection.commit();
            return entity;

        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public E completeByName(@NotNull final String userId,
                            @Nullable final String name) {
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        @NotNull final Connection connection =
                connectionService.getConnection();
        @NotNull final IBusinessRepository<E> entityRepository =
                getRepository(connection);
        try {
            @NotNull final E entity =
                    entityRepository.completeByName(userId, name);
            connection.commit();
            return entity;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<E> findAll(
            @NotNull final String userId,
            @Nullable final Comparator<E> comparator
    ) {
        Optional.ofNullable(comparator).orElse(null);
        @NotNull final Connection connection =
                connectionService.getConnection();
        @NotNull final IBusinessRepository<E> entityRepository =
                getRepository(connection);
        try {
            return entityRepository.findAll(userId, comparator);
        } finally {
            connection.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<E> findAll(@NotNull final String userId) {
        @NotNull final Connection connection =
                connectionService.getConnection();
        @NotNull final IBusinessRepository<E> entityRepository =
                getRepository(connection);
        try {
            return entityRepository.findAll(userId);
        } finally {
            connection.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public E findOneById(
            @NotNull final String userId, @Nullable final String id
    ) {
        Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        @NotNull final Connection connection =
                connectionService.getConnection();
        @NotNull final IBusinessRepository<E> entityRepository =
                getRepository(connection);
        try {
            return entityRepository.findOneById(userId, id);
        } finally {
            connection.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public E findOneByIndex(
            @NotNull final String userId, final Integer index
    ) {
        Optional.ofNullable(index).filter(item -> item > -1).
                orElseThrow(() -> new IndexIncorrectException(index));
        @NotNull final Connection connection =
                connectionService.getConnection();
        @NotNull final IBusinessRepository<E> entityRepository =
                getRepository(connection);
        try {
            return entityRepository.findOneByIndex(userId, index);
        } finally {
            connection.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public E findOneByName(
            @NotNull final String userId, final String name
    ) {
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        @NotNull final Connection connection =
                connectionService.getConnection();
        @NotNull final IBusinessRepository<E> entityRepository =
                getRepository(connection);
        try {
            return entityRepository.findOneByName(userId, name);
        } finally {
            connection.close();
        }
    }

    public abstract IBusinessRepository<E> getRepository(
            @NotNull Connection connection
    );

    @Nullable
    @Override
    @SneakyThrows
    public E removeOneById(
            @NotNull final String userId, final String id
    ) {
        Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        @NotNull final Connection connection =
                connectionService.getConnection();
        @NotNull final IBusinessRepository<E> entityRepository =
                getRepository(connection);
        try {
            @NotNull final E entity =
                    entityRepository.removeOneById(userId, id);
            connection.commit();
            return entity;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public E removeOneByIndex(
            @NotNull final String userId, final Integer index
    ) {
        Optional.ofNullable(index).filter(item -> item > -1).
                orElseThrow(() -> new IndexIncorrectException(index));
        @NotNull final Connection connection =
                connectionService.getConnection();
        @NotNull final IBusinessRepository<E> entityRepository =
                getRepository(connection);
        try {
            @Nullable final E entity =
                    entityRepository.removeOneByIndex(userId, index);
            connection.commit();
            return entity;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public E removeOneByName(
            @NotNull final String userId, @Nullable final String name
    ) {
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        @NotNull final Connection connection =
                connectionService.getConnection();
        @NotNull final IBusinessRepository<E> entityRepository =
                getRepository(connection);
        try {
            @Nullable final E entity =
                    entityRepository.removeOneByName(userId, name);
            connection.commit();
            return entity;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public E startById(
            @NotNull final String userId, @Nullable final String id
    ) {
        Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        final E entity = Optional.ofNullable(findOneById(userId, id))
                .orElseThrow(() -> new EntityNotFoundException(
                        this.getClass().getSimpleName()));
        entity.setStatus(Status.IN_PROGRESS.getDisplayName());
        entity.setDateStart(new Date());
        return entity;
    }

    @NotNull
    @Override
    @SneakyThrows
    public E startByIndex(
            @NotNull final String userId, @Nullable final Integer index
    ) {
        Optional.ofNullable(index).filter(item -> item > -1).
                orElseThrow(() -> new IndexIncorrectException(index));
        @NotNull final Connection connection =
                connectionService.getConnection();
        @NotNull final IBusinessRepository<E> entityRepository =
                getRepository(connection);
        try {
            @NotNull final E entity =
                    entityRepository.startByIndex(userId, index);
            connection.commit();
            return entity;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public E startByName(
            @NotNull final String userId, @Nullable final String name
    ) {
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        @NotNull final Connection connection =
                connectionService.getConnection();
        @NotNull final IBusinessRepository<E> entityRepository =
                getRepository(connection);
        try {
            @NotNull final E entity =
                    entityRepository.startByName(userId, name);
            connection.commit();
            return entity;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public E updateOneById(
            @NotNull final String userId, @Nullable final String id,
            @Nullable final String name, @Nullable final String description
    ) {
        Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        @NotNull final Connection connection =
                connectionService.getConnection();
        @NotNull final IBusinessRepository<E> entityRepository =
                getRepository(connection);
        try {
            @NotNull final E entity = entityRepository.updateOneById(
                    userId, id, name, description
            );
            connection.commit();
            return entity;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public E updateOneByIndex(
            @NotNull final String userId, @Nullable final Integer index,
            @Nullable final String name, @Nullable final String description
    ) {
        Optional.ofNullable(index).filter(item -> item > -1).
                orElseThrow(() -> new IndexIncorrectException(index));
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        @NotNull final Connection connection =
                connectionService.getConnection();
        @NotNull final IBusinessRepository<E> entityRepository =
                getRepository(connection);
        try {
            @NotNull final E entity = entityRepository.updateOneByIndex(
                    userId, index, name, description
            );
            connection.commit();
            return entity;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

}
