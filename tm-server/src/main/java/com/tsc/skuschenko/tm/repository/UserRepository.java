package com.tsc.skuschenko.tm.repository;

import com.tsc.skuschenko.tm.api.repository.IUserRepository;
import com.tsc.skuschenko.tm.constant.FieldConstant;
import com.tsc.skuschenko.tm.constant.TableConstant;
import com.tsc.skuschenko.tm.enumerated.Role;
import com.tsc.skuschenko.tm.exception.entity.user.UserNotFoundException;
import com.tsc.skuschenko.tm.model.User;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Optional;

public final class UserRepository extends AbstractRepository<User>
        implements IUserRepository {

    public UserRepository(@NotNull Connection connection) {
        super(connection);
    }

    @Override
    @SneakyThrows
    public void add(User user) {
        @NotNull final String query = "INSERT INTO " + getTableName() +
                "(id, email, firstName, lastName, locked, login, middleName, " +
                "passwordHash,role) values(?,?,?,?,?,?,?,?,?)";
        @NotNull final PreparedStatement statement =
                connection.prepareStatement(query);
        statement.setString(1, user.getId());
        statement.setString(2, user.getEmail());
        statement.setString(6, user.getLogin());
        statement.setString(9, user.getRole());
        statement.setBoolean(5, user.isLocked());
        statement.setString(3, user.getFirstName());
        statement.setString(4, user.getLastName());
        statement.setString(7, user.getMiddleName());
        statement.setString(8, user.getPasswordHash());
        statement.execute();
        statement.close();
    }

    @NotNull
    @Override
    @SneakyThrows
    public User create(
            @Nullable final String login, @Nullable final String password,
            @Nullable final String email
    ) {
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(password);
        user.setEmail(email);
        add(user);
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User create(
            @Nullable final String login, @Nullable final String password,
            @Nullable final Role role
    ) {
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(password);
        user.setRole(role.getDisplayName());
        add(user);
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User create(
            @Nullable final String login, @Nullable final String password
    ) {
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(password);
        add(user);
        return user;
    }

    @Override
    @SneakyThrows
    protected User fetch(@Nullable ResultSet row) {
        if (!Optional.ofNullable(row).isPresent()) return null;
        @NotNull final User user = new User();
        user.setEmail(row.getString(FieldConstant.EMAIL));
        user.setLogin(row.getString(FieldConstant.LOGIN));
        user.setId(row.getString(FieldConstant.ID));
        user.setRole(row.getString(FieldConstant.ROLE));
        user.setLocked(row.getBoolean(FieldConstant.LOCKED));
        user.setFirstName(row.getString(FieldConstant.FIRST_NAME));
        user.setLastName(row.getString(FieldConstant.LAST_NAME));
        user.setMiddleName(row.getString(FieldConstant.MIDDLE_NAME));
        user.setPasswordHash(row.getString(FieldConstant.PASSWORD_HASH));
        return user;
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByEmail(@NotNull final String email) {
        @NotNull final String query = "SELECT * FROM " + getTableName() +
                " WHERE email = ? LIMIT 1";
        @NotNull final PreparedStatement statement =
                connection.prepareStatement(query);
        statement.setString(1, email);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        final boolean hasNext = resultSet.next();
        if (!hasNext) return null;
        @Nullable final User user = fetch(resultSet);
        statement.close();
        return user;
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByLogin(@NotNull final String login) {
        @NotNull final String query = "SELECT * FROM " + getTableName() +
                " WHERE login = ? LIMIT 1";
        @NotNull final PreparedStatement statement =
                connection.prepareStatement(query);
        statement.setString(1, login);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        final boolean hasNext = resultSet.next();
        if (!hasNext) return null;
        @Nullable final User user = fetch(resultSet);
        statement.close();
        return user;
    }

    @Override
    protected @NotNull String getTableName() {
        return TableConstant.USER_TABLE;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User lockUserByLogin(@NotNull final String login) {
        @NotNull final User user = Optional.ofNullable(findByLogin(login))
                .orElseThrow(UserNotFoundException::new);
        user.setLocked(true);
        updateQuery(user);
        return user;
    }

    @Nullable
    @Override
    @SneakyThrows
    public User removeByLogin(@NotNull final String login) {
        final User user = findByLogin(login);
        @NotNull final String query = "DELETE FROM " + getTableName() +
                " WHERE login = ?";
        @NotNull final PreparedStatement statement =
                connection.prepareStatement(query);
        statement.setString(1, login);
        statement.executeUpdate();
        statement.close();
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User setPassword(
            @NotNull final String userId, @NotNull final String password
    ) {
        @NotNull final User user = Optional.ofNullable(findById(userId))
                .orElseThrow(UserNotFoundException::new);
        user.setPasswordHash(password);
        updateQuery(user);
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User unlockUserByLogin(@NotNull final String login) {
        @NotNull final User user = Optional.ofNullable(findByLogin(login))
                .orElseThrow(UserNotFoundException::new);
        user.setLocked(false);
        updateQuery(user);
        return user;
    }

    @Nullable
    @SneakyThrows
    private User updateQuery(@Nullable final User entity) {
        if (entity == null) return null;
        @NotNull final String query = "update " + getTableName() +
                "set id=?, email=?, login=?, role=?, locked=?, firstName=?, " +
                "lastName=?, middleName=?, passwordHash=?";
        @NotNull final PreparedStatement statement =
                connection.prepareStatement(query);
        statement.setString(1, entity.getId());
        statement.setString(2, entity.getEmail());
        statement.setString(3, entity.getLogin());
        statement.setString(4, entity.getRole());
        statement.setBoolean(5, entity.isLocked());
        statement.setString(6, entity.getFirstName());
        statement.setString(7, entity.getLastName());
        statement.setString(8, entity.getLastName());
        statement.setString(8, entity.getPasswordHash());
        statement.executeUpdate();
        statement.close();
        return entity;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User updateUser(
            @NotNull final String userId, @Nullable final String firstName,
            @Nullable final String lastName, @Nullable final String middleName
    ) {
        @NotNull final User user = Optional.ofNullable(findById(userId))
                .orElseThrow(UserNotFoundException::new);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        updateQuery(user);
        return user;
    }

}
