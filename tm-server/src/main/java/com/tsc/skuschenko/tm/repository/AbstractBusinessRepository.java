package com.tsc.skuschenko.tm.repository;

import com.tsc.skuschenko.tm.api.repository.IBusinessRepository;
import com.tsc.skuschenko.tm.enumerated.Status;
import com.tsc.skuschenko.tm.exception.empty.EmptyIdException;
import com.tsc.skuschenko.tm.exception.entity.EntityNotFoundException;
import com.tsc.skuschenko.tm.model.AbstractBusinessEntity;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.util.stream.Collectors;

public abstract class AbstractBusinessRepository<E extends AbstractBusinessEntity>
        extends AbstractRepository<E> implements IBusinessRepository<E> {

    protected AbstractBusinessRepository(@NotNull Connection connection) {
        super(connection);
    }

    @Override
    @SneakyThrows
    public void add(@NotNull E entity) {
        @NotNull final String query = "INSERT INTO " + getTableName() +
                "(id, dateBegin, dateEnd, description, name, user_id," +
                "status,created) VALUES(?,?,?,?,?,?,?,?)";
        @NotNull final PreparedStatement statement =
                connection.prepareStatement(query);
        statement.setString(1, entity.getId());
        statement.setString(5, entity.getName());
        statement.setString(4, entity.getDescription());
        statement.setString(7, entity.getStatus());
        statement.setDate(2, convertDate(entity.getDateStart()));
        statement.setDate(3, convertDate(entity.getDateFinish()));
        statement.setDate(8, convertDate(entity.getCreated()));
        statement.setString(6, entity.getUserId());
        statement.executeUpdate();
        statement.close();
    }

    @NotNull
    @Override
    @SneakyThrows
    public E changeStatusById(
            @NotNull final String userId, @NotNull final String id,
            @NotNull final Status status
    ) {
        @NotNull final E entity = Optional.ofNullable(findOneById(userId, id))
                .orElseThrow(() ->
                        new EntityNotFoundException(
                                this.getClass().getSimpleName()
                        )
                );
        entity.setStatus(status.getDisplayName());
        updateEntityQuery(entity);
        return entity;
    }

    @NotNull
    @Override
    @SneakyThrows
    public E changeStatusByIndex(
            @NotNull final String userId, @NotNull final Integer index,
            @NotNull final Status status
    ) {
        @NotNull final E entity =
                Optional.ofNullable(findOneByIndex(userId, index))
                        .orElseThrow(() -> new EntityNotFoundException(
                                        this.getClass().getSimpleName()
                                )
                        );
        entity.setStatus(status.getDisplayName());
        updateEntityQuery(entity);
        return entity;
    }

    @NotNull
    @Override
    @SneakyThrows
    public E changeStatusByName(
            @NotNull final String userId, @NotNull final String name,
            @NotNull final Status status
    ) {
        @NotNull final E entity =
                Optional.ofNullable(findOneByName(userId, name))
                        .orElseThrow(() -> new EntityNotFoundException(
                                this.getClass().getSimpleName()));
        updateEntityQuery(entity);
        return entity;
    }

    @Override
    @SneakyThrows
    public void clear(@NotNull final String userId) {
        @NotNull final String query = "DELETE FROM " + getTableName() +
                " WHERE user_id = ?";
        @NotNull final PreparedStatement statement =
                connection.prepareStatement(query);
        statement.setString(1, userId);
        statement.executeUpdate();
        statement.close();
    }

    @NotNull
    @Override
    @SneakyThrows
    public E completeById(
            @NotNull final String userId, @NotNull final String id
    ) {
        @NotNull final E entity = Optional.ofNullable(findOneById(userId, id))
                .orElseThrow(() -> new EntityNotFoundException(
                        this.getClass().getSimpleName()));
        entity.setDateFinish(new Date());
        entity.setStatus(Status.COMPLETE.getDisplayName());
        updateEntityQuery(entity);
        return entity;
    }

    @NotNull
    @Override
    @SneakyThrows
    public E completeByIndex(
            @NotNull final String userId, @NotNull final Integer index
    ) {
        @NotNull final E entity = Optional.ofNullable(
                findOneByIndex(userId, index)
        ).orElseThrow(() -> new EntityNotFoundException(
                this.getClass().getSimpleName()));
        entity.setStatus(Status.COMPLETE.getDisplayName());
        updateEntityQuery(entity);
        return entity;
    }

    @NotNull
    @Override
    @SneakyThrows
    public E completeByName(@NotNull final String userId,
                            @NotNull final String name) {
        @NotNull final E entity =
                Optional.ofNullable(findOneByName(userId, name))
                        .orElseThrow(() -> new EntityNotFoundException(
                                this.getClass().getSimpleName()));
        entity.setStatus(Status.COMPLETE.getDisplayName());
        updateEntityQuery(entity);
        return entity;
    }

    private java.sql.Date convertDate(@Nullable final Date date) {
        if (!Optional.ofNullable(date).isPresent()) return null;
        return new java.sql.Date(date.getTime());
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<E> findAll(@NotNull final String userId) {
        @NotNull final String query = "SELECT * FROM " + getTableName() +
                "WHERE id=?";
        @NotNull final PreparedStatement statement =
                connection.prepareStatement(query);
        statement.setString(1, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery(query);
        @NotNull final List<E> entities = new ArrayList<>();
        while (resultSet.next()) entities.add(fetch(resultSet));
        statement.close();
        return entities;
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<E> findAll(
            @NotNull final String userId,
            @NotNull final Comparator<E> comparator
    ) {
        @NotNull final String query = "SELECT * FROM " + getTableName() +
                "WHERE id=?";
        @NotNull final PreparedStatement statement =
                connection.prepareStatement(query);
        statement.setString(1, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery(query);
        @NotNull final List<E> entities = new ArrayList<>();
        while (resultSet.next()) entities.add(fetch(resultSet));
        statement.close();
        return entities.stream()
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @Nullable
    @Override
    @SneakyThrows
    public E findOneById(
            @NotNull final String userId, @NotNull final String id
    ) {
        @NotNull final String query = "SELECT * FROM " + getTableName() +
                " WHERE id = ? AND user_id=? LIMIT 1";
        @NotNull final PreparedStatement statement =
                connection.prepareStatement(query);
        statement.setString(1, id);
        statement.setString(2, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        final boolean hasNext = resultSet.next();
        if (!hasNext) return null;
        @NotNull final E entity = fetch(resultSet);
        statement.close();
        return entity;
    }

    @Nullable
    @Override
    @SneakyThrows
    public E findOneByIndex(
            @NotNull final String userId, @NotNull final Integer index
    ) {
        @NotNull final String query = "SELECT * FROM " + getTableName() +
                " WHERE user_id=? LIMIT 1 OFFSET ?";
        @NotNull final PreparedStatement statement =
                connection.prepareStatement(query);
        statement.setInt(2, index);
        statement.setString(1, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        final boolean hasNext = resultSet.next();
        if (!hasNext) return null;
        @NotNull final E entity = fetch(resultSet);
        statement.close();
        return entity;
    }

    @Nullable
    @Override
    @SneakyThrows
    public E findOneByName(
            @NotNull final String userId, @NotNull final String name
    ) {
        @NotNull final String query = "SELECT * FROM " + getTableName() +
                " WHERE user_id=? AND name=? LIMIT 1";
        @NotNull final PreparedStatement statement =
                connection.prepareStatement(query);
        statement.setString(2, name);
        statement.setString(1, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        final boolean hasNext = resultSet.next();
        if (!hasNext) return null;
        @NotNull final E entity = fetch(resultSet);
        statement.close();
        return entity;
    }

    @SneakyThrows
    private void removeEntityQuery(
            @NotNull final E entity
    ) {
        @NotNull final String query = "DELETE FROM " + getTableName() +
                " WHERE user_id = ?";
        @NotNull final PreparedStatement statement =
                connection.prepareStatement(query);
        statement.setString(1, entity.getUserId());
        statement.executeUpdate();
        statement.close();
    }

    @Nullable
    @Override
    @SneakyThrows
    public E removeOneById(
            @NotNull final String userId, @NotNull final String id
    ) {
        @Nullable final Optional<E> entity =
                Optional.ofNullable(findOneById(userId, id));
        removeEntityQuery(entity.get());
        return entity.get();
    }

    @Nullable
    @Override
    @SneakyThrows
    public E removeOneByIndex(
            @NotNull final String userId, @NotNull final Integer index
    ) {
        @Nullable final Optional<E> entity =
                Optional.ofNullable(findOneByIndex(userId, index));
        removeEntityQuery(entity.get());
        return entity.get();
    }

    @Nullable
    @Override
    @SneakyThrows
    public E removeOneByName(
            @NotNull final String userId, @NotNull final String name
    ) {
        @Nullable final Optional<E> entity
                = Optional.ofNullable(findOneByName(userId, name));
        removeEntityQuery(entity.get());
        return entity.get();
    }

    @NotNull
    @Override
    @SneakyThrows
    public E startById(
            @NotNull final String userId, @Nullable final String id
    ) {
        Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        @NotNull final E entity = Optional.ofNullable(findOneById(userId, id))
                .orElseThrow(() -> new EntityNotFoundException(
                        this.getClass().getSimpleName()));
        entity.setStatus(Status.IN_PROGRESS.getDisplayName());
        entity.setDateStart(new Date());
        updateEntityQuery(entity);
        return entity;
    }

    @NotNull
    @Override
    @SneakyThrows
    public E startByIndex(
            @NotNull final String userId, @NotNull final Integer index
    ) {
        @NotNull final E entity = Optional.ofNullable(
                findOneByIndex(userId, index)
        ).orElseThrow(() -> new EntityNotFoundException(
                this.getClass().getSimpleName()));
        entity.setStatus(Status.IN_PROGRESS.getDisplayName());
        entity.setDateStart(new Date());
        updateEntityQuery(entity);
        return entity;
    }

    @NotNull
    @Override
    @SneakyThrows
    public E startByName(
            @NotNull final String userId, @NotNull final String name
    ) {
        @NotNull final E entity =
                Optional.ofNullable(findOneByName(userId, name))
                        .orElseThrow(() -> new EntityNotFoundException(
                                this.getClass().getSimpleName()));
        entity.setStatus(Status.IN_PROGRESS.getDisplayName());
        entity.setDateStart(new Date());
        updateEntityQuery(entity);
        return entity;
    }

    @SneakyThrows
    public void updateEntityQuery(@NotNull final E entity) {
        @NotNull final String query = "UPDATE " + getTableName() +
                "SET id=?, dateBegin=?, dateEnd=?, description=?, name=?, " +
                "user_id=?, status=?, created=?";
        @NotNull final PreparedStatement statement =
                connection.prepareStatement(query);
        statement.setString(1, entity.getId());
        statement.setString(5, entity.getName());
        statement.setString(4, entity.getDescription());
        statement.setString(7, entity.getStatus());
        statement.setDate(2, (java.sql.Date) entity.getDateStart());
        statement.setDate(3, (java.sql.Date) entity.getDateFinish());
        statement.setDate(8, (java.sql.Date) entity.getCreated());
        statement.setString(6, entity.getUserId());
        statement.executeUpdate();
        statement.close();
    }

    @NotNull
    @Override
    @SneakyThrows
    public E updateOneById(
            @NotNull final String userId, @Nullable final String id,
            @Nullable final String name, @Nullable final String description
    ) {
        @NotNull final E entity = Optional.ofNullable(findOneById(userId, id))
                .orElseThrow(() -> new EntityNotFoundException(
                        this.getClass().getSimpleName()));
        entity.setName(name);
        entity.setDescription(description);
        updateEntityQuery(entity);
        return entity;
    }

    @NotNull
    @Override
    @SneakyThrows
    public E updateOneByIndex(
            @NotNull final String userId, @NotNull final Integer index,
            @Nullable final String name, @Nullable final String description
    ) {
        @NotNull final E entity = Optional.ofNullable(
                findOneByIndex(userId, index)
        )
                .orElseThrow(() -> new EntityNotFoundException(
                        this.getClass().getSimpleName()));
        entity.setName(name);
        entity.setDescription(description);
        updateEntityQuery(entity);
        return entity;
    }

}
