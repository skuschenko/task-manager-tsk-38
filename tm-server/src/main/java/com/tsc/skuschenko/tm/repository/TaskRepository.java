package com.tsc.skuschenko.tm.repository;

import com.tsc.skuschenko.tm.api.repository.ITaskRepository;
import com.tsc.skuschenko.tm.constant.FieldConstant;
import com.tsc.skuschenko.tm.constant.TableConstant;
import com.tsc.skuschenko.tm.model.Task;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public final class TaskRepository extends AbstractBusinessRepository<Task>
        implements ITaskRepository {

    public TaskRepository(@NotNull Connection connection) {
        super(connection);
    }

    @Override
    @SneakyThrows
    protected Task fetch(@Nullable ResultSet row) {
        if (!Optional.ofNullable(row).isPresent()) return null;
        @NotNull final Task task = new Task();
        task.setName(row.getString(FieldConstant.NAME));
        task.setDescription(row.getString(FieldConstant.DESCRIPTION));
        task.setId(row.getString(FieldConstant.ID));
        task.setUserId(row.getString(FieldConstant.USER_ID));
        task.setDateStart(row.getDate(FieldConstant.START_DATE));
        task.setDateFinish(row.getDate(FieldConstant.FINISH_DATE));
        task.setCreated(row.getDate(FieldConstant.CREATED));
        task.setProjectId(row.getString(FieldConstant.PROJECT_ID));
        task.setStatus(row.getString(FieldConstant.STATUS));
        return task;
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<Task> findAllTaskByProjectId(
            @NotNull final String userId, @NotNull final String projectId
    ) {
        @NotNull final String query = "SELECT * FROM " + getTableName() +
                " WHERE project_id = ? AND user_id=?";
        @NotNull final PreparedStatement statement =
                connection.prepareStatement(query);
        statement.setString(1, projectId);
        statement.setString(2, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        final boolean hasNext = resultSet.next();
        if (!hasNext) return null;
        @NotNull final List<Task> tasks = new ArrayList<>();
        while (resultSet.next()) tasks.add(fetch(resultSet));
        statement.close();
        return tasks;
    }

    @Override
    protected @NotNull String getTableName() {
        return TableConstant.TASK_TABLE;
    }

}
