package com.tsc.skuschenko.tm.api.service;


import java.sql.Connection;

public interface IConnectionService {

    Connection getConnection();

}
