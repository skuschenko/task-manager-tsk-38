package com.tsc.skuschenko.tm.repository;

import com.tsc.skuschenko.tm.api.IRepository;
import com.tsc.skuschenko.tm.model.AbstractEntity;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public abstract class AbstractRepository<E extends AbstractEntity>
        implements IRepository<E> {

    @NotNull
    protected final Connection connection;

    protected AbstractRepository(@NotNull final Connection connection) {
        this.connection = connection;
    }

    public abstract void add(@NotNull final E entity);

    @Override
    public void addAll(@Nullable final List<E> entities) {
        entities.forEach(this::add);
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final String query = "DELETE FROM " + getTableName();
        @NotNull final PreparedStatement statement =
                connection.prepareStatement(query);
        statement.executeUpdate();
        statement.close();
    }

    protected abstract E fetch(@Nullable final ResultSet row);

    @NotNull
    @Override
    @SneakyThrows
    public List<E> findAll() {
        @NotNull final Statement statement = connection.createStatement();
        @NotNull final String query = "SELECT * FROM " + getTableName();
        @NotNull final ResultSet resultSet = statement.executeQuery(query);
        @NotNull final List<E> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        statement.close();
        return result;
    }

    @Nullable
    @Override
    @SneakyThrows
    public E findById(@NotNull final String id) {
        @NotNull final String query = "SELECT * FROM " + getTableName() +
                " WHERE id=? LIMIT 1";
        @NotNull final PreparedStatement statement =
                connection.prepareStatement(query);
        statement.setString(1, id);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        final boolean hasNext = resultSet.next();
        if (!hasNext) return null;
        @NotNull final E result = fetch(resultSet);
        statement.close();
        return result;
    }

    protected abstract String getTableName();

    @Override
    @SneakyThrows
    public void remove(@NotNull final E entity) {
        @NotNull final String query = "DELETE FROM " + getTableName() +
                " WHERE id = ?";
        @NotNull final PreparedStatement statement =
                connection.prepareStatement(query);
        statement.setString(1, entity.getId());
        statement.executeUpdate();
        statement.close();
    }

    @Nullable
    @Override
    @SneakyThrows
    public E removeById(@NotNull final String id) {
        @Nullable final Optional<E> entity = Optional.ofNullable(findById(id));
        @NotNull final String query = "DELETE FROM " + getTableName() +
                " WHERE id = ?";
        @NotNull final PreparedStatement statement =
                connection.prepareStatement(query);
        statement.setString(1, id);
        statement.executeUpdate();
        statement.close();
        return entity.get();
    }

}
