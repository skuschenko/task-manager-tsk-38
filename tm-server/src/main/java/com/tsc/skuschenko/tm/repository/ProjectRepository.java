package com.tsc.skuschenko.tm.repository;

import com.tsc.skuschenko.tm.api.repository.IProjectRepository;
import com.tsc.skuschenko.tm.constant.FieldConstant;
import com.tsc.skuschenko.tm.constant.TableConstant;
import com.tsc.skuschenko.tm.model.Project;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.sql.Connection;
import java.sql.ResultSet;
import java.util.Optional;

public final class ProjectRepository extends AbstractBusinessRepository<Project>
        implements IProjectRepository {

    public ProjectRepository(@NotNull Connection connection) {
        super(connection);
    }

    @Override
    @SneakyThrows
    public Project fetch(@Nullable ResultSet row) {
        if (!Optional.ofNullable(row).isPresent()) return null;
        @NotNull final Project project = new Project();
        project.setName(row.getString(FieldConstant.NAME));
        project.setDescription(row.getString(FieldConstant.DESCRIPTION));
        project.setId(row.getString(FieldConstant.ID));
        project.setUserId(row.getString(FieldConstant.USER_ID));
        project.setDateStart(row.getDate(FieldConstant.START_DATE));
        project.setDateFinish(row.getDate(FieldConstant.FINISH_DATE));
        project.setCreated(row.getDate(FieldConstant.CREATED));
        project.setStatus(row.getString(FieldConstant.STATUS));
        return project;
    }

    protected String getTableName() {
        return TableConstant.PROJECT_TABLE;
    }

}
