package com.tsc.skuschenko.tm.service;

import com.tsc.skuschenko.tm.api.IRepository;
import com.tsc.skuschenko.tm.api.IService;
import com.tsc.skuschenko.tm.api.service.IConnectionService;
import com.tsc.skuschenko.tm.exception.empty.EmptyIdException;
import com.tsc.skuschenko.tm.exception.entity.EntityNotFoundException;
import com.tsc.skuschenko.tm.model.AbstractEntity;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.sql.Connection;
import java.util.List;
import java.util.Optional;


public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    @NotNull
    protected final IConnectionService connectionService;

    @NotNull
    public AbstractService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @Override
    @SneakyThrows
    public void add(@Nullable final E entity) {
        Optional.ofNullable(entity)
                .orElseThrow(() -> new EntityNotFoundException(
                        this.getClass().getSimpleName()));
        @NotNull final Connection connection =
                connectionService.getConnection();
        @NotNull final IRepository<E> entityRepository =
                getRepository(connection);
        try {
            entityRepository.add(entity);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public void addAll(@NotNull List<E> entity) {
        @NotNull final Connection connection =
                connectionService.getConnection();
        @NotNull final IRepository<E> entityRepository =
                getRepository(connection);
        try {
            entityRepository.addAll(entity);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final Connection connection =
                connectionService.getConnection();
        @NotNull final IRepository<E> entityRepository =
                getRepository(connection);
        try {
            entityRepository.clear();
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<E> findAll() {
        @NotNull final Connection connection =
                connectionService.getConnection();
        @NotNull final IRepository<E> entityRepository =
                getRepository(connection);
        try {
            return entityRepository.findAll();
        } finally {
            connection.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public E findById(@Nullable final String id) {
        Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        @NotNull final Connection connection =
                connectionService.getConnection();
        @NotNull final IRepository<E> entityRepository =
                getRepository(connection);
        try {
            return entityRepository.findById(id);
        } finally {
            connection.close();
        }
    }

    public abstract IRepository<E>
    getRepository(@NotNull Connection connection);

    @Override
    @SneakyThrows
    public void remove(@Nullable final E entity) {
        Optional.ofNullable(entity)
                .orElseThrow(() -> new EntityNotFoundException(
                        this.getClass().getSimpleName()));
        @NotNull final Connection connection =
                connectionService.getConnection();
        @NotNull final IRepository<E> entityRepository =
                getRepository(connection);
        try {
            entityRepository.remove(entity);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public E removeById(@Nullable final String id) {
        Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        @NotNull final Connection connection =
                connectionService.getConnection();
        @NotNull final IRepository<E> entityRepository =
                getRepository(connection);
        try {
            entityRepository.removeById(id);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return null;
    }

}
