package com.tsc.skuschenko.tm.service;

import com.tsc.skuschenko.tm.api.repository.ITaskRepository;
import com.tsc.skuschenko.tm.api.service.IConnectionService;
import com.tsc.skuschenko.tm.api.service.ITaskService;
import com.tsc.skuschenko.tm.exception.empty.EmptyDescriptionException;
import com.tsc.skuschenko.tm.exception.empty.EmptyNameException;
import com.tsc.skuschenko.tm.model.Project;
import com.tsc.skuschenko.tm.model.Task;
import com.tsc.skuschenko.tm.repository.TaskRepository;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.sql.Connection;
import java.util.Optional;

public final class TaskService extends AbstractBusinessService<Task>
        implements ITaskService {

    @NotNull
    private final IConnectionService connectionService;

    public TaskService(@NotNull IConnectionService connectionService) {
        super(connectionService);
        this.connectionService = connectionService;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task add(
            @Nullable final String userId, @Nullable final String name,
            @Nullable final String description
    ) {
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        Optional.ofNullable(description)
                .orElseThrow(EmptyDescriptionException::new);
        @NotNull final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        task.setUserId(userId);
        @NotNull final Connection connection =
                connectionService.getConnection();
        @NotNull final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        project.setUserId(userId);
        @NotNull final ITaskRepository taskRepository =
                getRepository(connection);
        try {
            taskRepository.add(task);
            connection.commit();
            return task;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    public ITaskRepository getRepository(@NotNull Connection connection) {
        return new TaskRepository(connection);
    }

}