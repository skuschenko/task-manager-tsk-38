package com.tsc.skuschenko.tm.api.other;

import org.jetbrains.annotations.NotNull;

public interface ISaltSetting {

    @NotNull
    Integer getPasswordIteration();

    @NotNull
    String getPasswordSecret();

}
