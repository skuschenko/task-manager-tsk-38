package com.tsc.skuschenko.tm.command.user;

import com.tsc.skuschenko.tm.endpoint.Session;
import com.tsc.skuschenko.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class UserLoginCommand extends AbstractUserCommand {

    @NotNull
    private static final String DESCRIPTION = "user login";

    @NotNull
    private static final String NAME = "login";

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        showOperationInfo(NAME);
        showParameterInfo("login");
        @NotNull final String login = TerminalUtil.nextLine();
        showParameterInfo("password");
        @NotNull final String password = TerminalUtil.nextLine();
        @NotNull final Session session =
                serviceLocator.getSessionEndpoint().openSession(login, password);
        serviceLocator.setSession(session);
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

}
